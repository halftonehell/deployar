# DeployAR

Tools and Pipeline for Handhend AR deployment

<div align="center">
    ![DeployAR thumbnail](readme/deployar-diagram-01.png){width=80%}
</div>

## About

This is proof-of-concept pipeline includes tools for quicker 3D asset prototyping for Unreal Engine [Handheld AR](https://docs.unrealengine.com/4.27/en-US/SharingAndReleasing/XRDevelopment/AR/ARHowTos/HandheldAR/).

It features tools and builds from my other repos:

<table style="margin-left:auto; margin-right:auto; text-align:center;">
<tbody>
<tr>
    <td>
        <a href="https://gitlab.com/halftonehell/assemblage">
            <img src="readme/thumbnail-assemblage-01.png"><br />
        </a>
        <p align="center"><a href="https://gitlab.com/halftonehell/assemblage">Assemblage</a>, Blender add-on
        </p>
    </td>
    <td>
        <a href="https://gitlab.com/halftonehell/decoupage">
            <img src="readme/thumbnail-decoupage-01.png"><br />
        </a>
        <p align="center"><a href="https://gitlab.com/halftonehell/decoupage">Decoupage</a>, Substance3D plugin
        </p>
    </td>
</tr>
<tr>
    <td>
        <a href="https://gitlab.com/halftonehell/Texturematica">
            <img src="readme/thumbnail-texturematica-01.png"><br />
        </a>
        <p align="center"><a href="https://gitlab.com/halftonehell/Texturematica">Texturematica</a>, Unreal Editor plugin
        </p>
    </td>
    <td>
        <a href="https://gitlab.com/halftonehell/deploymesh">
            <img src="readme/thumbnail-deploymesh-01.png"><br />
        </a>
        <p align="center"><a href="https://gitlab.com/halftonehell/deploymesh">DeployMesh</a>, UE Handheld AR Blueprints
        </p>
    </td>
</tr>
<tr>
    <td>
        <a href="https://gitlab.com/halftonehell/chiral">
            <img src="readme/thumbnail-chiral-01.png"><br />
        </a>
        <p align="center"><a href="https://gitlab.com/halftonehell/chiral">Chiral</a>, Perforce HelixCore Docker build
        </p>
    </td>
    <td>
        <a href="https://gitlab.com/halftonehell/worldpipe">
            <img src="readme/thumbnail-worldpipe-01.png"><br />
        </a>
        <p align="center"><a href="https://gitlab.com/halftonehell/worldpipe">Worldpipe</a>, TeamCity Docker build
        </p>
    </td>
</tr>
</tbody>
</table>

## Background

I built this workflow to help automate the toil out of the Unreal Engine course [Build a Detective's Office Game Environment](https://dev.epicgames.com/community/learning/courses/WK/unreal-engine-build-a-detective-s-office-game-environment/YOr/unreal-engine-introduction-to-the-detective-s-office-course).

## Requirements

See requirements for each repository.
